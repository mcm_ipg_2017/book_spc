package com.toymobi.book_spc_app;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.book_spc.BookSPCFrag;
import com.toymobi.recursos.BaseFragment;

public class BookSPCActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_book_spc);
        startBookFrag();
    }

    private void startBookFrag() {

        BookSPCFrag.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final BaseFragment fragmentBook = new BookSPCFrag();

        fragmentTransaction.replace(R.id.main_layout_book_spc_fragment_id,
                fragmentBook, BookSPCFrag.FRAGMENT_TAG_BOOK_SPC_TEXT);

        fragmentTransaction.commit();
    }
}
