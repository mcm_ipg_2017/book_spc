package com.toymobi.book_spc;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.toymobi.framework.adapter.ReducedCustomPagerAdapter;
import com.toymobi.framework.debug.DebugUtility;
import com.toymobi.framework.persistence.PersistenceManager;
import com.toymobi.framework.view.imageview.ImageViewAnimationTTS;
import com.toymobi.recursos.EduardoStuff;

class BookSPCController {

    private ViewPager viewPager;

    private ReducedCustomPagerAdapter adapter;

    int pageNumber;

    private CharSequence pageNumberText;

    private final Context context;

    private List<ImageViewAnimationTTS> imageViewSPC = null;

    MenuItem menuItemPageNumber;

    private static final String PERSISTENCE_KEY_PAGE = "PERSISTENCE_KEY_PAGE";

    TextToSpeech textToSpeech;

    BookSPCController(@NonNull final Context context,
                      @NonNull final LayoutInflater inflater) {
        DebugUtility.printDebug("BookController Construtor");

        this.context = context;

        start(inflater);
    }

    private void createAdapter(@NonNull final LayoutInflater layoutInflater) {

        if (context != null && adapter == null) {

            final int size = BookSPCResources.LAYOUTS_LENGTH;

            adapter = new ReducedCustomPagerAdapter(size);

            for (int i = 0; i < size; i++) {
                View temp = layoutInflater.inflate(BookSPCResources.BOOK_PAGES_LAYOUT[i], null);
                adapter.addViewPage(i, temp);
            }
            createItemsTTS();
        }
    }

    private void createViewPager() {
        if (adapter != null) {
            viewPager.setAdapter(adapter);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(final int page) {
                    pageNumber = page;

                    pageNumberText = "" + (pageNumber + 1);

                    if (menuItemPageNumber != null) {
                        menuItemPageNumber.setTitle(pageNumberText);
                    }
                }

                @Override
                public void onPageScrolled(final int page,
                                           final float positionOffset,
                                           final int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(final int position) {
                }
            });
        }
    }

    final void startBookText(final View view) {
        if (view != null) {
            viewPager = view.findViewById(R.id.viewPagerBook);
        }
        createViewPager();
    }

    private void goBookPage(final int page) {
        if (viewPager != null && pageNumber < BookSPCResources.LAYOUTS_LENGTH
                && page < BookSPCResources.LAYOUTS_LENGTH) {
            pageNumber = page;
            viewPager.setCurrentItem(pageNumber);
        }
    }

    final void bookPagePreviews() {
        if (viewPager != null) {
            if (pageNumber > 0) {
                --pageNumber;
                viewPager.setCurrentItem(pageNumber);
            } else {
                pageNumber = BookSPCResources.BOOK_PAGES_SIZE - 1;
                goBookPage(pageNumber);
            }
        }
    }

    final void bookPageNext() {
        if (viewPager != null) {
            if (pageNumber < BookSPCResources.LAYOUTS_LENGTH - 1) {
                ++pageNumber;
                viewPager.setCurrentItem(pageNumber);
            } else {
                pageNumber = 0;
                goBookPage(pageNumber);
            }
        }
    }


    void start(@NonNull final LayoutInflater inflater) {
        createAdapter(inflater);
    }

    final void changePageNumberText() {
        if (menuItemPageNumber != null) {
            if (pageNumber == BookSPCResources.BOOK_PAGES_LAYOUT.length) {
                pageNumberText = "" + BookSPCResources.BOOK_PAGES_LAYOUT.length;
            } else {
                pageNumberText = "" + (pageNumber + 1);
            }
            menuItemPageNumber.setTitle(pageNumberText);
        }
    }

    final void deallocate() {

        stopTTS();

        if (adapter != null) {

            for (int index = 0; index < BookSPCResources.BOOK_PAGES_LAYOUT.length; index++) {

                View viewTemp = adapter.getViewPage(index);

                if (viewTemp != null) {

                    ImageView picture = viewTemp.findViewById(R.id.book_page_layout_image);

                    if (picture != null) {
                        picture.setImageDrawable(null);
                        picture.setImageBitmap(null);
                    }
                }
            }
            adapter.deallocate();
            adapter = null;
        }

        if (viewPager != null) {
            viewPager.removeAllViews();
            viewPager.removeAllViewsInLayout();
        }
    }

    final void savePage() {
        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            PersistenceManager.saveSharedPreferencesInt(context, PERSISTENCE_KEY_PAGE, pageNumber);
        }
    }

    final void loadPage() {

        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            pageNumber = PersistenceManager.loadSharedPreferencesInt(context, PERSISTENCE_KEY_PAGE, 0);
        }
        goBookPage(pageNumber);
    }

    final void setTTS() {
        if (imageViewSPC != null && imageViewSPC.size() > 0) {

            final int length = imageViewSPC.size();

            for (int i = 0; i < length; i++) {
                final ImageViewAnimationTTS imageViewAnimationTTS = imageViewSPC.get(i);

                if (imageViewAnimationTTS != null) {
                    imageViewAnimationTTS.setTextToSpeech(textToSpeech);
                }
            }
        }
    }

    private void stopTTS() {
        if (EduardoStuff.ENABLE_TTS && textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private void createItemsTTS() {
        if (adapter != null) {
            if (imageViewSPC == null) {
                imageViewSPC = new ArrayList<>();

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_1_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(0).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_1[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_2_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(1).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_2[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_3_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(2).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_3[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_4_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(3).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_4[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_5_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(4).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_5[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_6_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(5).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_6[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_7_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = adapter.getViewPage(6).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_7[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                final ImageViewAnimationTTS temp = adapter.getViewPage(7).findViewById(R.id.imageViewSPC_8_1_1);

                if (temp != null) {
                    imageViewSPC.add(temp);
                }
            }
        }
    }
}
