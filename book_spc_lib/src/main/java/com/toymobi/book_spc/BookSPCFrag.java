package com.toymobi.book_spc;

import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.toymobi.recursos.BaseFragment;
import com.toymobi.recursos.EduardoStuff;

public class BookSPCFrag extends BaseFragment implements OnInitListener {

    private BookSPCController bookController;

    public static final String FRAGMENT_TAG_BOOK_SPC_TEXT = "BookSPCFrag";

    private static final int MY_DATA_CHECK_CODE = 1234;

    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createMusicBackground(R.string.path_sound_book, R.raw.book_sound, true);

        createSFX(R.raw.sfx_normal_click);

        createVibrationeedback();
    }

    @Override
    public void onResume() {

        super.onResume();

        if (bookController != null) {
            bookController.loadPage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (bookController != null) {
            bookController.savePage();
        }
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu,
                                          @NonNull final MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.menu_book_text, menu);

        // Set an icon in the ActionBar
        itemSound = menu.findItem(R.id.sound_menu_book);

        setIconSoundMenu();

        final MenuItem itemPageNumber = menu.findItem(R.id.number_page_menu_book);

        if (itemPageNumber != null) {
            bookController.menuItemPageNumber = itemPageNumber;
            bookController.changePageNumberText();
        }
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_book_spc, container, false);

        createToolBar(R.id.toolbar_book_spc, R.string.book_spc_toolbar_name);

        if (bookController == null) {

            final Context context = getContext();

            if (context != null) {
                bookController = new BookSPCController(context, inflater);

                // Fire off an intent to check if a TTS engine is installed
                final Intent checkIntent = new Intent();

                checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);

                startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
            }
        } else {
            bookController.start(inflater);
        }

        if (mainView != null) {
            bookController.startBookText(mainView);
        }
        return mainView;
    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int item_id = item.getItemId();

        playFeedBackButtons();

        if (item_id == R.id.back_menu_book) {

            exitOrBackMenu(startSingleApp);

        } else if (item_id == R.id.sound_menu_book) {

            changeSoundMenu();

        } else if (item_id == R.id.preview_page_menu_book) {

            if (bookController.pageNumber != 0) {
                bookController.bookPagePreviews();
            }

        } else if (item_id == R.id.next_page_menu_book) {

            if (bookController.pageNumber != BookSPCResources.LAYOUTS_LENGTH - 1) {
                bookController.bookPageNext();
            }

        } else {
            super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public void deallocate() {

        super.deallocate();

        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }

        if (bookController != null) {
            bookController.deallocate();
            bookController = null;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_DATA_CHECK_CODE && EduardoStuff.ENABLE_TTS) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {

                final Context context = getActivity();

                if (context != null) {
                    bookController.textToSpeech = new TextToSpeech(context, BookSPCFrag.this);
                }

            } else {
                final Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    @Override
    public void onInit(int status) {
        if (EduardoStuff.ENABLE_TTS && status == TextToSpeech.SUCCESS
                && bookController != null) {

            final Locale locateBR = new Locale("pt_BR");

            final int result = bookController.textToSpeech.setLanguage(locateBR);

            final Locale locatePT = new Locale("pt");

            final int result_2 = bookController.textToSpeech
                    .setLanguage(locatePT);

            if ((result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                    || (result_2 == TextToSpeech.LANG_MISSING_DATA || result_2 == TextToSpeech.LANG_NOT_SUPPORTED)) {
                EduardoStuff.ENABLE_TTS = false;
            } else {
                bookController.setTTS();
            }
        } else {
            EduardoStuff.ENABLE_TTS = false;
        }
    }
}
