package com.toymobi.book_spc;

interface BookSPCResources {

    int[] BOOK_PAGES_LAYOUT = {R.layout.book_card_01,
            R.layout.book_card_02, R.layout.book_card_03,
            R.layout.book_card_04, R.layout.book_card_05,
            R.layout.book_card_06, R.layout.book_card_07,
            R.layout.book_card_08};

    int BOOK_PAGES_SIZE = BOOK_PAGES_LAYOUT.length;

    int LAYOUTS_LENGTH = BOOK_PAGES_LAYOUT.length;

    int[] SPC_BUTTONS_PAGE_1 = {R.id.imageViewSPC_1_1_1,
            R.id.imageViewSPC_1_1_2, R.id.imageViewSPC_1_1_3,
            R.id.imageViewSPC_1_1_4, R.id.imageViewSPC_1_1_5,
            R.id.imageViewSPC_1_1_6, R.id.imageViewSPC_1_1_7,
            R.id.imageViewSPC_1_1_8, R.id.imageViewSPC_1_1_9,
            R.id.imageViewSPC_1_2_1, R.id.imageViewSPC_1_2_2,
            R.id.imageViewSPC_1_2_3, R.id.imageViewSPC_1_2_4,
            R.id.imageViewSPC_1_2_5, R.id.imageViewSPC_1_2_6,
            R.id.imageViewSPC_1_2_7, R.id.imageViewSPC_1_3_1,
            R.id.imageViewSPC_1_3_2, R.id.imageViewSPC_1_3_3};

    int[] SPC_BUTTONS_PAGE_2 = {R.id.imageViewSPC_2_1_1,
            R.id.imageViewSPC_2_1_2, R.id.imageViewSPC_2_1_3,
            R.id.imageViewSPC_2_1_4, R.id.imageViewSPC_2_2_1,
            R.id.imageViewSPC_2_2_2, R.id.imageViewSPC_2_2_3,
            R.id.imageViewSPC_2_2_1, R.id.imageViewSPC_2_2_2,
            R.id.imageViewSPC_2_2_4, R.id.imageViewSPC_2_2_5,
            R.id.imageViewSPC_2_2_6, R.id.imageViewSPC_2_2_7,
            R.id.imageViewSPC_2_2_8, R.id.imageViewSPC_2_3_1,
            R.id.imageViewSPC_2_3_5, R.id.imageViewSPC_2_3_6,
            R.id.imageViewSPC_2_3_2, R.id.imageViewSPC_2_3_3,
            R.id.imageViewSPC_2_3_4, R.id.imageViewSPC_2_3_5,
            R.id.imageViewSPC_2_3_6};

    int[] SPC_BUTTONS_PAGE_3 = {R.id.imageViewSPC_3_1_1,
            R.id.imageViewSPC_3_1_2, R.id.imageViewSPC_3_1_3,
            R.id.imageViewSPC_3_1_4, R.id.imageViewSPC_3_1_5,
            R.id.imageViewSPC_3_1_6, R.id.imageViewSPC_3_2_1,
            R.id.imageViewSPC_3_2_2, R.id.imageViewSPC_3_2_3,
            R.id.imageViewSPC_3_3_1, R.id.imageViewSPC_3_3_2,
            R.id.imageViewSPC_3_3_3, R.id.imageViewSPC_3_3_4,
            R.id.imageViewSPC_3_3_5, R.id.imageViewSPC_3_3_6,
            R.id.imageViewSPC_3_3_7, R.id.imageViewSPC_3_4_1,
            R.id.imageViewSPC_3_4_2, R.id.imageViewSPC_3_4_3,
            R.id.imageViewSPC_3_4_4, R.id.imageViewSPC_3_4_5,
            R.id.imageViewSPC_3_4_6, R.id.imageViewSPC_3_4_7};

    int[] SPC_BUTTONS_PAGE_4 = {R.id.imageViewSPC_4_1_1,
            R.id.imageViewSPC_4_1_2, R.id.imageViewSPC_4_1_3,
            R.id.imageViewSPC_4_1_4, R.id.imageViewSPC_4_1_5,
            R.id.imageViewSPC_4_1_6, R.id.imageViewSPC_4_1_7,
            R.id.imageViewSPC_4_2_1, R.id.imageViewSPC_4_2_2,
            R.id.imageViewSPC_4_2_3, R.id.imageViewSPC_4_2_4,
            R.id.imageViewSPC_4_3_1, R.id.imageViewSPC_4_3_2,
            R.id.imageViewSPC_4_3_3, R.id.imageViewSPC_4_3_4,
            R.id.imageViewSPC_4_3_5, R.id.imageViewSPC_4_3_6,
            R.id.imageViewSPC_4_3_7, R.id.imageViewSPC_4_3_8,
            R.id.imageViewSPC_4_4_1, R.id.imageViewSPC_4_4_2,
            R.id.imageViewSPC_4_4_3, R.id.imageViewSPC_4_4_4,
            R.id.imageViewSPC_4_4_5, R.id.imageViewSPC_4_4_6,
            R.id.imageViewSPC_4_4_7, R.id.imageViewSPC_4_4_8,
            R.id.imageViewSPC_4_5_1, R.id.imageViewSPC_4_5_2,
            R.id.imageViewSPC_4_5_3, R.id.imageViewSPC_4_5_4,
            R.id.imageViewSPC_4_5_5, R.id.imageViewSPC_4_5_6,
            R.id.imageViewSPC_4_5_7, R.id.imageViewSPC_4_5_8};

    int[] SPC_BUTTONS_PAGE_5 = {R.id.imageViewSPC_5_1_1,
            R.id.imageViewSPC_5_1_2, R.id.imageViewSPC_5_1_3,
            R.id.imageViewSPC_5_1_4, R.id.imageViewSPC_5_1_5,
            R.id.imageViewSPC_5_2_1, R.id.imageViewSPC_5_2_2,
            R.id.imageViewSPC_5_2_3, R.id.imageViewSPC_5_2_4,
            R.id.imageViewSPC_5_2_5, R.id.imageViewSPC_5_2_6,
            R.id.imageViewSPC_5_2_7, R.id.imageViewSPC_5_3_1,
            R.id.imageViewSPC_5_3_2, R.id.imageViewSPC_5_3_3,
            R.id.imageViewSPC_5_3_4, R.id.imageViewSPC_5_3_5,
            R.id.imageViewSPC_5_3_6};

    int[] SPC_BUTTONS_PAGE_6 = {R.id.imageViewSPC_6_1_1,
            R.id.imageViewSPC_6_1_2, R.id.imageViewSPC_6_1_3,
            R.id.imageViewSPC_6_1_4, R.id.imageViewSPC_6_1_5,
            R.id.imageViewSPC_6_1_6, R.id.imageViewSPC_6_2_1,
            R.id.imageViewSPC_6_2_2, R.id.imageViewSPC_6_2_3,
            R.id.imageViewSPC_6_2_4, R.id.imageViewSPC_6_2_5,
            R.id.imageViewSPC_6_2_6, R.id.imageViewSPC_6_2_7};

    int[] SPC_BUTTONS_PAGE_7 = {R.id.imageViewSPC_7_1_1,
            R.id.imageViewSPC_7_1_2, R.id.imageViewSPC_7_1_3,
            R.id.imageViewSPC_7_1_4, R.id.imageViewSPC_7_2_1,
            R.id.imageViewSPC_7_2_2, R.id.imageViewSPC_7_2_3,
            R.id.imageViewSPC_7_2_4, R.id.imageViewSPC_7_2_5,
            R.id.imageViewSPC_7_3_1, R.id.imageViewSPC_7_3_2,
            R.id.imageViewSPC_7_3_3, R.id.imageViewSPC_7_3_4,
            R.id.imageViewSPC_7_3_5, R.id.imageViewSPC_7_3_6,
            R.id.imageViewSPC_7_3_7, R.id.imageViewSPC_7_3_8,
            R.id.imageViewSPC_7_3_9, R.id.imageViewSPC_7_4_1,
            R.id.imageViewSPC_7_4_2, R.id.imageViewSPC_7_5_1,
            R.id.imageViewSPC_7_5_2, R.id.imageViewSPC_7_5_3,
            R.id.imageViewSPC_7_5_4, R.id.imageViewSPC_7_5_5,
            R.id.imageViewSPC_7_5_6, R.id.imageViewSPC_7_5_7,
            R.id.imageViewSPC_7_5_8};

    int SPC_BUTTONS_PAGE_1_LENGTH = SPC_BUTTONS_PAGE_1.length;
    int SPC_BUTTONS_PAGE_2_LENGTH = SPC_BUTTONS_PAGE_2.length;
    int SPC_BUTTONS_PAGE_3_LENGTH = SPC_BUTTONS_PAGE_3.length;
    int SPC_BUTTONS_PAGE_4_LENGTH = SPC_BUTTONS_PAGE_4.length;
    int SPC_BUTTONS_PAGE_5_LENGTH = SPC_BUTTONS_PAGE_5.length;
    int SPC_BUTTONS_PAGE_6_LENGTH = SPC_BUTTONS_PAGE_6.length;
    int SPC_BUTTONS_PAGE_7_LENGTH = SPC_BUTTONS_PAGE_7.length;

}
